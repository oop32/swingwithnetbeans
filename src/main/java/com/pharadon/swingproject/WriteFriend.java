/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pharadon.swingproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.text.html.ObjectView;

/**
 *
 * @author pond
 */
public class WriteFriend {
    public static void main(String[] args) {
        ArrayList<Friend> friends = new ArrayList<>();
        friends.add(new Friend("Pharadon", 20, "Male", "NO des"));
        friends.add(new Friend("Phon", 20, "Male", "NO des"));
        friends.add(new Friend("Pharn", 21, "Male", "NO des"));
        
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            file = new File("friend.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(friends);
            oos.close();
        }catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE,null,ex);
        }catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
}
