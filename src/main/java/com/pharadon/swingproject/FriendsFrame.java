/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.pharadon.swingproject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.*;

/**
 *
 * @author pond
 */
public class FriendsFrame extends javax.swing.JFrame {

    /**
     * Creates new form FriendsFrame
     */
    public FriendsFrame() {
        friendList = new ArrayList<>();
        loadFriends();
        model = new DefaultListModel<>();
        initComponents();
        disableForm();
        refreshList();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        lblage = new javax.swing.JPanel();
        lblname = new javax.swing.JLabel();
        txtname = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtage = new javax.swing.JFormattedTextField();
        lblgender = new javax.swing.JLabel();
        lbldescription = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtdescription = new javax.swing.JTextPane();
        dropdowngender = new javax.swing.JComboBox<>();
        Clearbutton = new javax.swing.JButton();
        Savebutton = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        editbutton = new javax.swing.JButton();
        addbutton = new javax.swing.JButton();
        deletebutton = new javax.swing.JButton();

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jList1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblage.setBackground(new java.awt.Color(255, 255, 51));

        lblname.setText("Name");

        txtname.setToolTipText("");

        jLabel4.setText("Age");

        txtage.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));

        lblgender.setText("Gender");

        lbldescription.setText("Description");

        jScrollPane1.setViewportView(txtdescription);

        dropdowngender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Male", "Female" }));
        dropdowngender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dropdowngenderActionPerformed(evt);
            }
        });

        Clearbutton.setBackground(new java.awt.Color(102, 102, 255));
        Clearbutton.setText("Clear");
        Clearbutton.setActionCommand("Save");
        Clearbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClearbuttonActionPerformed(evt);
            }
        });

        Savebutton.setBackground(new java.awt.Color(153, 153, 0));
        Savebutton.setText("Save");
        Savebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SavebuttonActionPerformed(evt);
            }
        });

        jList2.setModel(model);
        jScrollPane3.setViewportView(jList2);

        editbutton.setText("Edit");
        editbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editbuttonActionPerformed(evt);
            }
        });

        addbutton.setText("Add");
        addbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addbuttonActionPerformed(evt);
            }
        });

        deletebutton.setText("Delete");
        deletebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletebuttonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout lblageLayout = new javax.swing.GroupLayout(lblage);
        lblage.setLayout(lblageLayout);
        lblageLayout.setHorizontalGroup(
            lblageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lblageLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(lblageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(lblageLayout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 581, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(lblageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(addbutton)
                            .addComponent(editbutton)
                            .addComponent(deletebutton)))
                    .addGroup(lblageLayout.createSequentialGroup()
                        .addComponent(lblname)
                        .addGap(18, 18, 18)
                        .addComponent(txtname, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(txtage, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblgender)
                        .addGap(18, 18, 18)
                        .addComponent(dropdowngender, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(lblageLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 581, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(lblageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Clearbutton)
                            .addComponent(Savebutton)))
                    .addComponent(lbldescription))
                .addContainerGap())
        );
        lblageLayout.setVerticalGroup(
            lblageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lblageLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(lblageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblname)
                    .addComponent(txtname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txtage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblgender)
                    .addComponent(dropdowngender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbldescription)
                .addGap(12, 12, 12)
                .addGroup(lblageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(lblageLayout.createSequentialGroup()
                        .addComponent(Savebutton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Clearbutton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(lblageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(lblageLayout.createSequentialGroup()
                        .addComponent(addbutton)
                        .addGap(18, 18, 18)
                        .addComponent(editbutton)
                        .addGap(18, 18, 18)
                        .addComponent(deletebutton)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 6, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void dropdowngenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dropdowngenderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dropdowngenderActionPerformed

    private void ClearbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClearbuttonActionPerformed
        clearForm();        // TODO add your handling code here:
    }//GEN-LAST:event_ClearbuttonActionPerformed

    private void SavebuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SavebuttonActionPerformed
        
        String name = txtname.getText(); 
        if(name.length()<3) {
            JOptionPane.showMessageDialog(this, "Name length bust be least 3 character");
            txtname.requestFocus();
            return;
        }
        
        int age = 0;
        try {
            age = Integer.parseInt(txtage.getText());
        } catch(NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "age must be int");
            txtage.requestFocus();
            return;
           
        }
        
                
        String gender = dropdowngender.getSelectedItem().toString();
        String description = txtdescription.getText();
        
        if(age<10) {
            JOptionPane.showMessageDialog(this, "Age must be least 10");
            return;
        }
        
        Friend friend = new Friend(name,age,gender,description);
        if(friendid<0){
            friendList.add(friend);
        }else{
            friendList.set(friendid, friend);
        }
        System.out.println(friend);
        saveFriends();
        refreshList();
        clearForm();
        disableForm();
        
    }//GEN-LAST:event_SavebuttonActionPerformed

    private void deletebuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletebuttonActionPerformed
        int id = jList2.getSelectedIndex();
        if(id<0) return;
        System.out.print(id);
        friendList.remove(id);
        saveFriends();
        refreshList();
        clearForm();
        disableForm();
    }//GEN-LAST:event_deletebuttonActionPerformed

    private void editbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editbuttonActionPerformed
        friendid = jList2.getSelectedIndex();
        if(friendid<0) return;
        Friend friend = friendList.get(friendid);
        txtname.setText(friend.getName());
        txtage.setText(""+friend.getAge());
        dropdowngender.setSelectedItem(friend.getGender());
        txtdescription.setText(friend.getDescription());
        enableForm();
    }//GEN-LAST:event_editbuttonActionPerformed

    private void addbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addbuttonActionPerformed
        enableForm();
        clearForm();
        
    }//GEN-LAST:event_addbuttonActionPerformed

    private void refreshList() {
        model.clear();
        for(int i = 0;i<friendList.size();i++){
            model.addElement(friendList.get(i).toString());
        }
    }
    private void disableForm(){
        txtname.setEnabled(false);
        txtage.setEnabled(false);
        dropdowngender.setEnabled(false);
        txtdescription.setEnabled(false);
        Savebutton.setEnabled(false);
        txtdescription.setEnabled(false);
        Clearbutton.setEnabled(false);
    }
    
    private void enableForm(){
        txtname.setEnabled(true);
        txtage.setEnabled(true);
        dropdowngender.setEnabled(true);
        txtdescription.setEnabled(true);
        Savebutton.setEnabled(true);
        txtdescription.setEnabled(true);
        addbutton.setEnabled(true);
        editbutton.setEnabled(true);
       
        
    }
    public void clearForm(){
        txtname.setText("");
        txtage.setText("");
        dropdowngender.setSelectedIndex(0);
        txtdescription.setText("");
        friendid = -1;
        txtname.requestFocus();
    }
    private void saveFriends() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            file = new File("friend.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(friendList);
            oos.close();
        }catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE,null,ex);
        }catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
    
    private void loadFriends() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("friend.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            friendList = (ArrayList<Friend>) ois.readObject();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /** 
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FriendsFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FriendsFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FriendsFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FriendsFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FriendsFrame().setVisible(true);
            }
        });
    }
    private int friendid = -1;
    private DefaultListModel<String> model;
    private ArrayList<Friend> friendList;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Clearbutton;
    private javax.swing.JButton Savebutton;
    private javax.swing.JButton addbutton;
    private javax.swing.JButton deletebutton;
    private javax.swing.JComboBox<String> dropdowngender;
    private javax.swing.JButton editbutton;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JList<String> jList1;
    private javax.swing.JList<String> jList2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel lblage;
    private javax.swing.JLabel lbldescription;
    private javax.swing.JLabel lblgender;
    private javax.swing.JLabel lblname;
    private javax.swing.JFormattedTextField txtage;
    private javax.swing.JTextPane txtdescription;
    private javax.swing.JTextField txtname;
    // End of variables declaration//GEN-END:variables
}
